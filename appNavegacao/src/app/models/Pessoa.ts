export class Pessoa {
    id: number = 0
    nome: string = ""
    telefone: string = ""
    data_cadastro: Date = new Date()
}