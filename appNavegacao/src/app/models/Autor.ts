import { Pessoa } from "./Pessoa";

export class Autor extends Pessoa{
    nacionalidade: string
    data_morte: Date
}