import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Autor } from 'src/app/models/Autor';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {
  private readonly URL_E = "https://3000-amandamello-2egaapi0810-8xz0fu351wu.ws-us78.gitpod.io/"
  private readonly URL_A = "https://3000-amandamello-2egaapi0810-8xz0fu351wu.ws-us78.gitpod.io/"
  private readonly URL_I = "https://3000-amandamello-2egaapi0810-8xz0fu351wu.ws-us78.gitpod.io/"
  private readonly URL = this.URL_A

  constructor(
    private http: HttpClient
  ) { }

  cadastrarAutor(autor: Autor): Observable<any> {
    return this.http.post<any>(`${this.URL}autor-e-pessoa`, autor)
  }

  buscarTodosOsAutores(): Observable<any> {
    return this.http.get<any>(`${this.URL}autores`)
  }

  buscarAutoresPeloNome(nome: string): Observable<any> {
    return this.http.get<any>(`${this.URL}autores/pornome/${nome}`)
  }
}
