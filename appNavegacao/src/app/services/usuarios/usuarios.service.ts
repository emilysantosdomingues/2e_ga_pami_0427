import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from 'src/app/models/Usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private readonly URL_I = "https://3000-amandamello-2egaapi0810-ugltvq4llnj.ws-us78.gitpod.io/"
  private readonly URL_E = "https://3000-amandamello-2egaapi0810-8xz0fu351wu.ws-us78.gitpod.io/"
  private readonly URL_A = "https://3000-amandamello-2egaapi0810-ugltvq4llnj.ws-us78.gitpod.io/"
  private readonly URL = this.URL_A

  constructor(
    private http: HttpClient
  ) { }

  logar(usuario: Usuario): Observable<any> {
    return this.http.post<any>(`${this.URL}login`, usuario)
  }

  listarUsuarios(): Observable<any> {
    return this.http.get<any>(`${this.URL}usuarios`)
  }

  cadastrarUsuario(usuario: Usuario): Observable<any> {
    return this.http.post<any>(`${this.URL}usuario`, usuario)
  }
}
