import { Component } from '@angular/core';
import { Autor } from '../models/Autor';
import { AutoresService } from '../services/autores/autores.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  rotas = [
    {
      path: "/cad-autor",
      text: "Cadastrar Autor"
    },
    {
      path: "/cad-editora",
      text: "Cadastrar Editora"
    },
    {
      path: "/cad-livro",
      text: "Cadastrar Livro"
    }
  ]
  
  autor: Autor

  constructor(
    private autoresService: AutoresService
  ) {
    /*this.autoresService.buscarTodosOsAutores()
    .subscribe(
      (dados) => {
        console.log(dados)
      },
      (error) => {
        console.error(error)
      }
    )

    this.autoresService.buscarAutoresPeloNome('ana')
    .subscribe(
      (dados) => {
        console.log(dados)
      },
      (error) => {
        console.error(error)
      }
    )*/

    this.autor = new Autor()

    this.autor.nome = ""
    this.autor.telefone = ""
    this.autor.nacionalidade = ""
    this.autor.data_cadastro = new Date()
    this.autor.data_morte = null

    this.autoresService.cadastrarAutor(this.autor)
    .subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}