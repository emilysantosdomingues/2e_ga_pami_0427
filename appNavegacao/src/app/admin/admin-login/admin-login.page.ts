import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.page.html',
  styleUrls: ['./admin-login.page.scss'],
})
export class AdminLoginPage implements OnInit {
  usuario: Usuario

  constructor(
    private usuariosService: UsuariosService
  ) { 
    this.usuario = new Usuario()
  }

  ngOnInit() {
  }

  logar(): void {
    console.log(this.usuario)

    this.usuariosService.logar(this.usuario).subscribe({
      next: (dados) => {
        console.log(dados)
      }, 
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
