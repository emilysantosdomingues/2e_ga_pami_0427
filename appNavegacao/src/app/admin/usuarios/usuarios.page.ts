import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.page.html',
  styleUrls: ['./usuarios.page.scss'],
})
export class UsuariosPage implements OnInit {
  usuarios: Usuario[]

  constructor(
    private usuariosService: UsuariosService
  ) { }

  ngOnInit() {
    this.usuariosService.listarUsuarios().subscribe({
      next: (dados) => {
        console.log(dados)
        this.usuarios = dados.dados
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}
