import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-cad-usuario',
  templateUrl: './cad-usuario.page.html',
  styleUrls: ['./cad-usuario.page.scss'],
})
export class CadUsuarioPage implements OnInit {
  usuario: Usuario

  constructor(
    private usuariosService: UsuariosService
  ) { 
    this.usuario = new Usuario()
  }

  ngOnInit() {
  }

  salvar(): void {
    this.usuariosService.cadastrarUsuario(this.usuario).subscribe({
      next: (dados) => {
        console.log(dados)
      }, 
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
